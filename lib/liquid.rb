# Copyright (c) 2005 Tobias Luetke
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

module Liquid
  FilterSeparator             = /\|/
  ArgumentSeparator           = ','
  ArgumentStart               = /\(/
  ArgumentEnd                 = /\)/
  ArgumentFragment            = /(?:#{ArgumentStart})(.*)(?:#{ArgumentEnd})/o
  FilterArgumentSeparator     = ':'
  VariableAttributeSeparator  = '.'
  TagStart                    = /\{\%/
  TagEnd                      = /\%\}/
  VariableSignature           = /\(?[\w\-\.\[\]]\)?/
  VariableSegment             = /[\w\-]/
  VariableStart               = /\{\{/
  VariableEnd                 = /\}\}/
  VariableIncompleteEnd       = /\}\}?/
  QuotedString                = /"[^"]*"|'[^']*'/
  QuotedFragment              = /#{QuotedString}|(?:[^\s,\|'"]|#{QuotedString})+/o
  VariableQuotedFragment      = /#{QuotedString}|(?:[^\s,\(\)\|'"]|#{QuotedString})+/o
  StrictQuotedFragment        = /"[^"]+"|'[^']+'|[^\s|:,]+/
  FirstFilterArgument         = /#{FilterArgumentSeparator}(?:#{StrictQuotedFragment})/o
  OtherFilterArgument         = /#{ArgumentSeparator}(?:#{StrictQuotedFragment})/o
  SpacelessFilter             = /^(?:'[^']+'|"[^"]+"|[^'"])*#{FilterSeparator}(?:#{StrictQuotedFragment})(?:#{FirstFilterArgument}(?:#{OtherFilterArgument})*)?/o
  Expression                  = /(?:#{QuotedFragment}(?:#{SpacelessFilter})*)/o
  TagAttributes               = /(\w+)\s*\:\s*(#{QuotedFragment})/o
  AnyStartingTag              = /\{\{|\{\%/
  PartialTemplateParser       = /#{TagStart}.*?#{TagEnd}|#{VariableStart}.*?#{VariableIncompleteEnd}/o
  TemplateParser              = /(#{PartialTemplateParser}|#{AnyStartingTag})/o
  VariableParser              = /\[[^\]]+\]|#{VariableSegment}+\??/o
end

require File.expand_path(File.join(File.dirname(__FILE__), "liquid/version"))
require File.expand_path(File.join(File.dirname(__FILE__), 'liquid/lexer'))
require File.expand_path(File.join(File.dirname(__FILE__), 'liquid/parser'))
require File.expand_path(File.join(File.dirname(__FILE__), 'liquid/i18n'))
require File.expand_path(File.join(File.dirname(__FILE__), 'liquid/drop'))
require File.expand_path(File.join(File.dirname(__FILE__), 'liquid/extensions'))
require File.expand_path(File.join(File.dirname(__FILE__), 'liquid/errors'))
require File.expand_path(File.join(File.dirname(__FILE__), 'liquid/interrupts'))
require File.expand_path(File.join(File.dirname(__FILE__), 'liquid/strainer'))
require File.expand_path(File.join(File.dirname(__FILE__), 'liquid/context'))
require File.expand_path(File.join(File.dirname(__FILE__), 'liquid/tag'))
require File.expand_path(File.join(File.dirname(__FILE__), 'liquid/block'))
require File.expand_path(File.join(File.dirname(__FILE__), 'liquid/document'))
require File.expand_path(File.join(File.dirname(__FILE__), 'liquid/variable'))
require File.expand_path(File.join(File.dirname(__FILE__), 'liquid/file_system'))
require File.expand_path(File.join(File.dirname(__FILE__), 'liquid/template'))
require File.expand_path(File.join(File.dirname(__FILE__), 'liquid/htmltags'))
require File.expand_path(File.join(File.dirname(__FILE__), 'liquid/standardfilters'))
require File.expand_path(File.join(File.dirname(__FILE__), 'liquid/condition'))
require File.expand_path(File.join(File.dirname(__FILE__), 'liquid/module_ex'))
require File.expand_path(File.join(File.dirname(__FILE__), 'liquid/utils'))

# Load all the tags of the standard library
#
Dir[File.dirname(__FILE__) + '/liquid/tags/*.rb'].each { |f| require f }
